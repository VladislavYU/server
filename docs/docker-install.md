# Установка Docker

1. Официальная документация: https://docs.docker.com/engine/install/.
2. Docker Desktop: https://docs.docker.com/desktop/.

## Ubuntu

Официальная документация: https://docs.docker.com/engine/install/ubuntu/.

```bash
sudo apt-get update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```

Кроме того, рекомендуется установить:
```bash
apt install git nano htop apache2-utils
```

### Создание пользователя и настройка прав

Дальнешие шаги, по мотивам [официальной документации docker](https://docs.docker.com/engine/install/linux-postinstall/):

1. Создать пользователя (например, `bitrix`), под которым будет происходить правка файлов на хост-машине / запускаться CI/CD.
2. Создать группу пользователей `docker`.
3. Добавить пользователя `bitrix` в группу `docker`.

В итоге в `/etc/group` должны получить что-то типа:

```config
docker:x:999:bitrix
bitrix:x:1000:
```

В дальнейшем идентификатор пользователя `bitrix` понадобится для настройки окружения.

## MacOS

### Официальный Docker
Официальная документация: https://docs.docker.com/desktop/install/mac-install/.
Достаточно скачать соответствующий вашему процессору дистрибутив и запустить установку.

## Windows

Для Windows можно установить только Docker Desktop: https://docs.docker.com/desktop/install/windows-install/.

# Другие решения
- https://orbstack.dev/ - по отзывам сообщества работает быстрее родного.
- https://www.portainer.io - значительно удобнее Docker Desktop под Ubuntu.
